#%%
%run ~/interactive/spark.py

#%%

"""
Main observations:
- there are a significant number of wikipedia pageviews (~17%) from google private prefetch proxy ip addresses https://developer.chrome.com/blog/private-prefetch-proxy
    - these pageviews do not result in any other requests from the user directly to wikipedia servers (the term "prefetch" is somewhat confusing)
    - google's blogpost states: From past experiments, we know that this feature typically results in less than 2% extra requests for main resources (for example HTML documents).
    - google publishes the subnets for the ips used by google private proxy
- of the pageviews coming from google proxy ips, ~57% are classified as automated
    - this is due to the fingerprint based mechanisms for bot detection, as the traffic comes from few ips (1639 in this analsyis) and the user agents are simplified
    - this means we partially include google proxy requests in the pageview statistics
- the vast majority of google prefetch originiate from chrome on Android (86%)
    - the global market share of android is 70% https://gs.statcounter.com/os-market-share/mobile/worldwide
    - for wmf pageviews classified as user: 64.6%
    - if we include the pageviews tagged as automated coming from google proxy servers: 69.7%
- the hypothesis that we wrongly classify a significant number of real pageviews as automated still stands. More investigation is warranted.

Suggestions:
    - To trigger google private proxy requests and analyse the scenarios
        - Needs to happen on Android or Windows
    - Google private proxy is only used if there is no cookie, but wikipedia is using cookies
        - Why are there so many requests from Android? could it be e.g. the search bar on the android homescreen?
    - More analysis
        - what is the relationship with google referred requests? why are there so many of these from Chrome than others?
        - this analysis is based only on a single hour of pageview actor data, expand/verfify
    - Google private proxy requests are not cached by google
        - If anything we are receiving more traffic as a result (e.g. as not all requests are actual pageviews)
        - Can we get more information from google about the nature of these requests? Can they tell us what percentage is humans loading a wikipedia page?
        - Can/should we disable google private proxy as an experiment?

"""

# Input data
year=2023
month=12
day=6
hour=12

pa = (spark.table("wmf.pageview_actor")
.where(f"""year={year} and month={month} and day={day} and hour={hour}""")
).cache()

wr = (spark.table("wmf.webrequest")
.where(f"""year={year} and month={month} and day={day} and hour={hour}""")
).cache()
#%%

#%%
pa = (pa
.where(F.col("page_id").isNotNull())
# not intersted in spider traffic for this analysis
.where(F.col("agent_type").isin(["user","automated"]))
# column to tag google referred pagewiews
.withColumn("google_referred", F.col("referer")=="https://www.google.com/")
.withColumn("is_chrome", F.col("user_agent_map").getItem("browser_family").contains("Chrome"))
.withColumn("os_family",F.col("user_agent_map").getItem("os_family"))
.withColumn("os_family",F
    .when(F.col("os_family") == "Android", "Android")
    .when(F.col("os_family") == "iOS", "iOS")
    .otherwise("other"))
)

# Google private proxy prefetching

# ips used by google for reverse proxy are public
# https://www.gstatic.com/chrome/prefetchproxy/prefetch_proxy_geofeed
ips = ["72.14.201.0","72.14.201.8","72.14.201.16","72.14.201.24","72.14.201.32","72.14.201.40","72.14.201.44","72.14.201.48","72.14.201.52","72.14.201.54","72.14.201.56","72.14.201.58","72.14.201.60","72.14.201.62","72.14.201.64","72.14.201.66","72.14.201.67","72.14.201.68","72.14.201.69","72.14.201.70","72.14.201.71","72.14.201.72","72.14.201.73","72.14.201.74","72.14.201.75","72.14.201.76","72.14.201.77","72.14.201.78","72.14.201.79","72.14.201.80","72.14.201.81","72.14.201.82","72.14.201.83","72.14.201.84","72.14.201.85","72.14.201.86","72.14.201.87","72.14.201.88","72.14.201.89","72.14.201.90","72.14.201.91","72.14.201.92","72.14.201.93","72.14.201.94","72.14.201.95","72.14.201.96","72.14.201.97","72.14.201.98","72.14.201.99","72.14.201.100","72.14.201.101","72.14.201.102","72.14.201.103","72.14.201.104","72.14.201.128","72.14.201.136","72.14.201.144","72.14.201.148","72.14.201.152","72.14.201.154","72.14.201.156","72.14.201.158","72.14.201.159","72.14.201.160","72.14.201.161","72.14.201.162","72.14.201.163","72.14.201.164","72.14.201.165","72.14.201.166","72.14.201.167","72.14.201.168","72.14.201.169","72.14.201.192","72.14.201.200","72.14.201.204","72.14.201.208","72.14.201.210","72.14.201.212","72.14.201.214","72.14.201.215","72.14.201.216","72.14.201.217","72.14.201.218","72.14.201.219","72.14.201.220","72.14.201.221","72.14.201.222","72.14.201.223","193.186.4.0","193.186.4.8","193.186.4.16","193.186.4.24","193.186.4.32","193.186.4.40","193.186.4.44","193.186.4.48","193.186.4.52","193.186.4.54","193.186.4.56","193.186.4.58","193.186.4.60","193.186.4.62","193.186.4.64","193.186.4.66","193.186.4.67","193.186.4.68","193.186.4.69","193.186.4.70","193.186.4.71","193.186.4.72","193.186.4.73","193.186.4.74","193.186.4.75","193.186.4.76","193.186.4.77","193.186.4.78","193.186.4.79","193.186.4.80","193.186.4.81","193.186.4.82","193.186.4.83","193.186.4.84","193.186.4.85","193.186.4.86","193.186.4.87","193.186.4.88","193.186.4.89","193.186.4.90","193.186.4.91","193.186.4.92","193.186.4.93","193.186.4.94","193.186.4.95","193.186.4.96","193.186.4.97","193.186.4.98","193.186.4.99","193.186.4.100","193.186.4.101","193.186.4.102","193.186.4.103","193.186.4.104","193.186.4.128","193.186.4.136","193.186.4.144","193.186.4.148","193.186.4.152","193.186.4.154","193.186.4.156","193.186.4.158","193.186.4.159","193.186.4.160","193.186.4.161","193.186.4.162","193.186.4.163","193.186.4.164","193.186.4.165","193.186.4.166","193.186.4.167","193.186.4.168","193.186.4.169","193.186.4.192","193.186.4.200","193.186.4.204","193.186.4.208","193.186.4.210","193.186.4.212","193.186.4.214","193.186.4.215","193.186.4.216","193.186.4.217","193.186.4.218","193.186.4.219","193.186.4.220","193.186.4.221","193.186.4.222","193.186.4.223","2001:4860:7:200::","2001:4860:7:201::","2001:4860:7:202::","2001:4860:7:203::","2001:4860:7:204::","2001:4860:7:205::","2001:4860:7:206::","2001:4860:7:207::","2001:4860:7:208::","2001:4860:7:209::","2001:4860:7:20a::","2001:4860:7:20b::","2001:4860:7:20c::","2001:4860:7:20d::","2001:4860:7:20e::","2001:4860:7:20f::","2001:4860:7:210::","2001:4860:7:211::","2001:4860:7:212::","2001:4860:7:213::","2001:4860:7:214::","2001:4860:7:215::","2001:4860:7:216::","2001:4860:7:217::","2001:4860:7:218::","2001:4860:7:219::","2001:4860:7:21a::","2001:4860:7:21b::","2001:4860:7:21c::","2001:4860:7:21d::","2001:4860:7:21e::","2001:4860:7:21f::","2001:4860:7:220::","2001:4860:7:221::","2001:4860:7:222::","2001:4860:7:223::","2001:4860:7:224::","2001:4860:7:225::","2001:4860:7:226::","2001:4860:7:227::","2001:4860:7:228::","2001:4860:7:229::","2001:4860:7:22a::","2001:4860:7:22b::","2001:4860:7:22c::","2001:4860:7:22d::","2001:4860:7:22e::","2001:4860:7:22f::","2001:4860:7:230::","2001:4860:7:231::","2001:4860:7:232::","2001:4860:7:233::","2001:4860:7:234::","2001:4860:7:235::","2001:4860:7:300::","2001:4860:7:301::","2001:4860:7:302::","2001:4860:7:303::","2001:4860:7:304::","2001:4860:7:305::","2001:4860:7:306::","2001:4860:7:307::","2001:4860:7:308::","2001:4860:7:309::","2001:4860:7:30a::","2001:4860:7:30b::","2001:4860:7:30c::","2001:4860:7:30d::","2001:4860:7:30e::","2001:4860:7:30f::","2001:4860:7:400::","2001:4860:7:401::","2001:4860:7:402::","2001:4860:7:403::","2001:4860:7:404::","2001:4860:7:405::","2001:4860:7:406::","2001:4860:7:407::","2001:4860:7:408::","2001:4860:7:409::","2001:4860:7:40a::","2001:4860:7:40b::","2001:4860:7:40c::","2001:4860:7:40d::","2001:4860:7:40e::","2001:4860:7:40f::","2001:4860:7:410::","2001:4860:7:411::","2001:4860:7:412::","2001:4860:7:500::","2001:4860:7:501::","2001:4860:7:502::","2001:4860:7:503::","2001:4860:7:504::","2001:4860:7:505::","2001:4860:7:506::","2001:4860:7:507::","2001:4860:7:508::","2001:4860:7:509::","2001:4860:7:50a::","2001:4860:7:50b::","2001:4860:7:50c::","2001:4860:7:50d::","2001:4860:7:50e::","2001:4860:7:50f::","2001:4860:7:510::","2001:4860:7:511::","2001:4860:7:512::","2001:4860:7:600::","2001:4860:7:601::","2001:4860:7:602::","2001:4860:7:603::","2001:4860:7:604::","2001:4860:7:605::","2001:4860:7:606::","2001:4860:7:607::","2001:4860:7:608::","2001:4860:7:609::","2001:4860:7:60a::","2001:4860:7:60b::","2001:4860:7:60c::","2001:4860:7:60d::","2001:4860:7:60e::","2001:4860:7:60f::","2001:4860:7:610::","2001:4860:7:611::","2001:4860:7:612::","2001:4860:7:613::","2001:4860:7:614::","2001:4860:7:615::","2001:4860:7:616::","2001:4860:7:617::","2001:4860:7:618::","2001:4860:7:619::","2001:4860:7:61a::","2001:4860:7:61b::","2001:4860:7:61c::","2001:4860:7:61d::","2001:4860:7:61e::","2001:4860:7:61f::","2001:4860:7:620::","2001:4860:7:621::","2001:4860:7:622::","2001:4860:7:623::","2001:4860:7:624::","2001:4860:7:625::","2001:4860:7:626::","2001:4860:7:627::","2001:4860:7:628::","2001:4860:7:629::","2001:4860:7:62a::","2001:4860:7:62b::","2001:4860:7:62c::","2001:4860:7:62d::","2001:4860:7:62e::","2001:4860:7:62f::","2001:4860:7:630::","2001:4860:7:631::","2001:4860:7:632::","2001:4860:7:633::","2001:4860:7:634::","2001:4860:7:635::","2001:4860:7:700::","2001:4860:7:701::","2001:4860:7:702::","2001:4860:7:703::","2001:4860:7:704::","2001:4860:7:705::","2001:4860:7:706::","2001:4860:7:707::","2001:4860:7:708::","2001:4860:7:709::","2001:4860:7:70a::","2001:4860:7:70b::","2001:4860:7:70c::","2001:4860:7:70d::","2001:4860:7:70e::","2001:4860:7:70f::"]
@F.udf(returnType="boolean")
def google_private_proxy(ip):
    return any([sub_ip in ip for sub_ip in ips])

pa = pa.withColumn("google_proxy", google_private_proxy("ip"))


#%%
# Google referred pageviews
p = (pa
.groupby(
    F.col("is_chrome"),
    F.col("google_referred"),
)
.count()
.orderBy('is_chrome', 'google_referred')
).show(100,truncate=False)

"""
- overall, 44% of pageviews are referred from google
- overall, the chrome accounts for 62% of pageview traffic, which is
matches the market share is 61.8%; https://www.browserstack.com/guide/understanding-browser-market-share
- mobile os matches market shares - 70% android 30% iOS (disregarding others); https://gs.statcounter.com/os-market-share/mobile/worldwide
- at the same time, chrome refers more requests via google.com.
    - 21.4% of non-chrome pageviews are referred by google
    - 58.7% of chrome pageviews are referred by google
- the extend of this seems suprising and warrants a deeper look
    - maybe due to automated traffic from google private proxy?
"""


# +---------+--------+
# |os_family|count   |
# +---------+--------+
# |Android  |12126639|
# |other    |10501844|
# |iOS      |5201547 |
# +---------+--------+

# +---------+--------+
# |is_chrome|count   |
# +---------+--------+
# |true     |17529091|
# |false    |10300939|
# +---------+--------+
# +---------------+--------+
# |google_referred|count   |
# +---------------+--------+
# |true           |12505129|
# |false          |15324901|
# +---------------+--------+
# +---------+---------------+--------+
# |is_chrome|google_referred|count   |
# +---------+---------------+--------+
# |false    |false          |8091807 |
# |false    |true           |2209132 |
# |true     |false          |7233094 |
# |true     |true           |10295997|
# +---------+---------------+--------+


#%%
# Automated pageviews referred by google
"""
Pageviews classified as automated:
- overall:
    - 13.1% of non-chrome traffic is classified as automated
    - 20.6% of chrome traffic is labelled as automated
- for pageview referred from google:
    - 0.2% of non-chrome traffic is classified as automated
    - 27% of chrome traffic is labelled as automated
- for pageview not referred from google:
    - 16.5% of non-chrome traffic is classified as automated
    - 11.7% of chrome traffic is labelled as automated

The large difference in automated traffic for google referred pageviews warrents a deeper look. It is likely due to both
- the google reverse proxy
- the fact that chrome is the source of a lot more google referred pageviews

"""
p = (pa
.groupby(
    F.col("google_referred"),
    F.col("agent_type"),
    F.col("is_chrome"),
)
.count()
.orderBy('google_referred', 'agent_type', 'is_chrome')
).show(100,truncate=False)
#%%

p = (pa
.groupby(
    F.col("os_family"),
    F.col("agent_type"),
)
.count()
.orderBy('os_family', 'agent_type')
).show(100,truncate=False)

# +---------------+----------+---------+-------+
# |google_referred|agent_type|is_chrome|count  |
# +---------------+----------+---------+-------+
# |false          |automated |false    |1343091|
# |false          |automated |true     |848119 |
# |false          |user      |false    |6748716|
# |false          |user      |true     |6384975|
# |true           |automated |false    |4544   |
# |true           |automated |true     |2777852|
# |true           |user      |false    |2204588|
# |true           |user      |true     |7518145|
# +---------------+----------+---------+-------+

# +---------------+----------+--------+
# |google_referred|agent_type|count   |
# +---------------+----------+--------+
# |false          |automated |2191210 |
# |false          |user      |13133691|
# |true           |automated |2782396 |
# |true           |user      |9722733 |
# +---------------+----------+--------+

# +----------+---------+--------+
# |agent_type|is_chrome|count   |
# +----------+---------+--------+
# |automated |false    |1347635 |
# |automated |true     |3625971 |
# |user      |false    |8953304 |
# |user      |true     |13903120|
# +----------+---------+--------+

#%%

# Google private proxy

"""
Pageviews & google private proxy
- 17.7% of pageviews are via google reverse proxy
- 28.1% of chrome pageviews are via google reverse proxy
- all traffic from google reverse proxy is from chrome
    - Android is responsible for 86.8%, Windows for 11.4%
    - 93.5% of google proxy requests are google.com referred

Automated pageviews from google proxy servers on Chrome
- For non-google proxy pageviews, 6.1% are automated
- For google proxy pageviews, 57.8% are automated

"""
#%%
p = (pa
.groupBy("is_chrome", "agent_type")
.count()
).show(1000,truncate=False)


#%%
p = (pa
.groupBy("google_proxy", "is_chrome")
.count()
).show(1000,truncate=False)

# +------------+---------+--------+
# |google_proxy|is_chrome|count   |
# +------------+---------+--------+
# |false       |true     |12594147|
# |true        |true     |4934944 |
# |false       |false    |10300939|
# +------------+---------+--------+
# 4934944/(4934944+12594147)
# 4934944/(4934944+12594147+10300939)
#%%
p = (pa
.groupBy("google_proxy", "agent_type")
.count()
.orderBy('google_proxy')
).show(1000,truncate=False)
# +------------+----------+--------+
# |google_proxy|agent_type|count   |
# +------------+----------+--------+
# |false       |automated |2117510 |
# |false       |user      |20777576|
# |true        |automated |2856096 |
# |true        |user      |2078848 |
# +------------+----------+--------+
2117510/(2117510+20777576)
2856096/(2856096+2078848)
#%%
p = (pa
.where(F.col("is_chrome"))
.groupBy("google_proxy", "agent_type")
.count()
.orderBy('google_proxy')
).show(1000,truncate=False)
# +------------+----------+--------+
# |google_proxy|agent_type|count   |
# +------------+----------+--------+
# |false       |automated |769875  |
# |false       |user      |11824272|
# |true        |automated |2856096 |
# |true        |user      |2078848 |
# +------------+----------+--------+
769875/(769875+11824272)

1347635/(1347635+8953304)
#%%
p = (pa
.where(F.col("is_chrome"))
.groupBy("google_proxy","google_referred")
.count()
.orderBy('google_proxy',"google_referred")
).show(1000,truncate=False)
# +------------+---------------+-------+
# |google_proxy|google_referred|count  |
# +------------+---------------+-------+
# |false       |false          |6916706|
# |false       |true           |5677441|
# |true        |false          |316388 |
# |true        |true           |4618556|
# +------------+---------------+-------+
# 4618556/(316388+4618556)
#%%
p = (pa
.groupBy("google_proxy","is_chrome","google_referred")
.agg(
    (F.sum(F.when(F.col("agent_type") == "automated", 1).otherwise(0)) / F.count("agent_type") * 100).alias("pautomated"),
    F.count("*").alias("total")
)
.orderBy("google_proxy","is_chrome","google_referred")
).show(1000,truncate=False)
# +------------+---------+---------------+-------------------+-------+
# |google_proxy|is_chrome|google_referred|pautomated         |total  |
# +------------+---------+---------------+-------------------+-------+
# |false       |false    |false          |16.598159100927642 |8091807|
# |false       |false    |true           |0.20569164721709704|2209132|
# |false       |true     |false          |11.01080485421818  |6916706|
# |false       |true     |true           |0.14601648876668205|5677441|
# |true        |true     |false          |27.35059483924801  |316388 |
# |true        |true     |true           |59.96597204840647  |4618556|
# +------------+---------+---------------+-------------------+-------+
#%%
p = (pa
.where(F.col("google_referred"))
.groupBy("google_proxy","is_chrome")
.agg(
    (F.sum(F.when(F.col("agent_type") == "automated", 1).otherwise(0)) / F.count("agent_type") * 100).alias("pautomated"),
    F.count("*").alias("total")
)
.orderBy('google_proxy',"is_chrome")
).show(1000,truncate=False)
# +------------+---------+-------------------+-------+
# |google_proxy|is_chrome|pautomated         |total  |
# +------------+---------+-------------------+-------+
# |false       |false    |0.20569164721709704|2209132|
# |false       |true     |0.14601648876668205|5677441|
# |true        |true     |59.96597204840647  |4618556|
# +------------+---------+-------------------+-------+

#%%
p = (pa
.groupBy("os_family")
.agg(
    (F.sum(F.when(F.col("agent_type") == "automated", 1).otherwise(0)) / F.count("agent_type") * 100).alias("pautomated"),
    F.sum(F.when(F.col("agent_type") == "user", 1).otherwise(0)).alias("user"),
    F.count("*").alias("total")
)
# )
.orderBy("os_family")
).show(1000,truncate=False)
# +---------+------------------+-------+--------+
# |os_family|pautomated        |user   |total   |
# +---------+------------------+-------+--------+
# |Android  |21.74990943492257 |9489106|12126639|
# |iOS      |0.3705051593304838|5182275|5201547 |
# |other    |22.0608971148305  |8185043|10501844|
# +---------+------------------+-------+--------+
"""
The market share of Android pageviews:
    - based on pageviews that are classified as user: 64.6%
    - if we add the pageviews tagged as automated coming from google proxy servers: 69.7%

"""
12126639/(5201547+12126639)
9489106/(5182275+9489106)

(9489106+(4287047-1801227))/(5182275+9489106+(4287047-1801227))

#%%
p = (pa
.groupBy("os_family","is_chrome")
.agg(
    (F.sum(F.when(F.col("agent_type") == "automated", 1).otherwise(0)) / F.count("agent_type") * 100).alias("pautomated"),
    F.sum(F.when(F.col("agent_type") == "user", 1).otherwise(0)).alias("user"),
    F.count("*").alias("total")
)
# )
.orderBy("os_family","is_chrome")
).show(1000,truncate=False)
# +---------+---------+-------------------+-------+--------+
# |os_family|is_chrome|pautomated         |user   |total   |
# +---------+---------+-------------------+-------+--------+
# |Android  |false    |2.6980390868712334 |947905 |974189  |
# |Android  |true     |23.414128734044986 |8541201|11152450|
# |iOS      |false    |0.22052621504568443|4602884|4613057 |
# |iOS      |true     |1.5461605124980884 |579391 |588490  |
# |other    |false    |27.81636394224231  |3402515|4713693 |
# |other    |true     |17.37382110452889  |4782528|5788151 |
# +---------+---------+-------------------+-------+--------+


#%%
p = (pa
.where(F.col("os_family").isin(["Android","iOS"]))
.groupBy("os_family","is_chrome","google_proxy")
.agg(
    (F.sum(F.when(F.col("agent_type") == "automated", 1).otherwise(0)) / F.count("agent_type") * 100).alias("pautomated"),
    F.sum(F.when(F.col("agent_type") == "user", 1).otherwise(0)).alias("user"),
    F.count("*").alias("total")
)
# )
.orderBy("os_family","is_chrome","google_proxy")
).show(1000,truncate=False)
# +---------+---------+------------+-------------------+-------+-------+
# |os_family|is_chrome|google_proxy|pautomated         |user   |total  |
# +---------+---------+------------+-------------------+-------+-------+
# |Android  |false    |false       |2.6980390868712334 |947905 |974189 |
# |Android  |true     |false       |1.8269721384163464 |6739974|6865403|
# |Android  |true     |true        |57.98443544005932  |1801227|4287047|
# |iOS      |false    |false       |0.22052621504568443|4602884|4613057|
# |iOS      |true     |false       |1.5461605124980884 |579391 |588490 |
# +---------+---------+------------+-------------------+-------+-------+
1801227+6739974
4287047-1801227
#%%